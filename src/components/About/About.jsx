import React from "react";

import AboutText from "../AboutText/AboutText";
import GalleryItem from "../GalleryItem/GalleryItem";

import Truck from "../../assets/truck.jpg";

import "./About.scss";

function About() {
  return (
    <section className="about">
      <h2 className="about-title">About us</h2>
      <div className="about-container">
        <AboutText />
        <GalleryItem image={Truck} />
      </div>
    </section>
  );
}

export default About;
