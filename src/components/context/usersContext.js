import { createContext, useState } from "react";

const UsersContext = createContext();

export const UsersProvider = ({ children }) => {
  const [users, setUsers] = useState([]);
  const [currentUser, setCurrentUser] = useState("");
  const [allOrders, setAllOrders] = useState([]);

  const checkEmail = (data, signInUser) => {
    const user = data.find((user) => user.email === signInUser.email);
    if (user) {
      return user;
    }
  };

  const addUser = async (newUser) => {
    const response = await fetch("http://localhost:5000/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    });

    const data = await response.json(newUser);

    setUsers([data]);

    setCurrentUser(data);
  };

  const curUser = async (signInUser) => {
    const response = await fetch(`http://localhost:5000/users`);

    const data = await response.json();

    const user = checkEmail(data, signInUser);

    localStorage.setItem("curUser", JSON.stringify(user));

    setCurrentUser(user);
  };

  const handleSignOut = () => {
    localStorage.clear(curUser);
    setCurrentUser("");
  };

  const addOrder = async (newOrder) => {
    const response = await fetch(`http://localhost:5000/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newOrder),
    });

    const data = await response.json();

    setAllOrders([data, ...allOrders]);
  };

  const curUserOrder = async () => {
    const response = await fetch(
      "http://localhost:5000/orders?_sort=id&_order=desc"
    );
    const data = await response.json();

    setAllOrders(data);
  };

  return (
    <UsersContext.Provider
      value={{
        users,
        currentUser,
        allOrders,
        addUser,
        curUser,
        addOrder,
        handleSignOut,
        curUserOrder,
      }}
    >
      {children}
    </UsersContext.Provider>
  );
};

export default UsersContext;
