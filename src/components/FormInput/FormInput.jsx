import React from "react";

import "./FormInput.scss";

function FormInput({ label, ...otherProps }) {
  return (
    <div className="input-group">
      <input className="input" {...otherProps} />
      <label className="label" htmlFor={otherProps.id}>
        {label}
      </label>
    </div>
  );
}

export default FormInput;
