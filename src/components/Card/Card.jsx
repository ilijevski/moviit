import React from "react";

import "./Card.scss";

function Card({ children, dark }) {
  return <div className={`card ${dark && "dark"}`}>{children}</div>;
}

export default Card;
