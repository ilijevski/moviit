import React from "react";

import GalleryItem from "../GalleryItem/GalleryItem";

import Truck1 from "../../assets/truck 2.jpg";
import Truck2 from "../../assets/truck 3.jpg";
import Truck3 from "../../assets/truck 4.jpg";
import Truck4 from "../../assets/truck 5.jpg";
import Truck5 from "../../assets/truck 6.jpg";
import Truck6 from "../../assets/price-truck.jpg";

import "./Gallery.scss";

function Gallery() {
  return (
    <div className="gallery">
      <h2 className="gallery-header">Gallery</h2>
      <div className="gallery-container">
        <GalleryItem image={Truck1} />
        <GalleryItem image={Truck3} />
        <GalleryItem image={Truck2} />
        <GalleryItem image={Truck4} />
        <GalleryItem image={Truck5} />
        <GalleryItem image={Truck6} />
      </div>
    </div>
  );
}

export default Gallery;
