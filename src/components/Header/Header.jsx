import React from "react";

import "./Header.scss";

function Header() {
  return (
    <div className="header">
      <div className="header-container">
        <h1 className="header-heading-primery">Make it Happen</h1>
        <p className="header-heading-para">
          Let us take the load and make your moving effortless
        </p>
      </div>
    </div>
  );
}

export default Header;
