import React from "react";

import "./Button.scss";

function Button({ children, type }) {
  return (
    <button type={type} className="btn">
      {children}
    </button>
  );
}

export default Button;
