import React from "react";

import "./FormInputCheckbox.scss";

function FormInputCheckbox({ label, ...otherProps }) {
  return (
    <div className="input-radio-group">
      <input type="checkbox" className="input-radio" {...otherProps} />

      <label className="label-radio" htmlFor={otherProps.id}>
        <span className="button-radio"></span>
        {label}
      </label>
    </div>
  );
}

export default FormInputCheckbox;
