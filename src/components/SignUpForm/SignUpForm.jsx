import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import Card from "../Card/Card";
import Button from "../Button/Button";
import FormInput from "../FormInput/FormInput";
import UsersContext from "../context/usersContext";
import ErrorModal from "../ErrorModal/ErrorModal";

const defaultFormFields = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  confirmPassword: "",
};

function SignUpForm() {
  const navigate = useNavigate();
  const [formFields, setFormFields] = useState(defaultFormFields);
  const { addUser, curUser, users } = useContext(UsersContext);
  const [error, setError] = useState("");
  const { firstName, lastName, email, password, confirmPassword } = formFields;

  const handleChange = (event) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!firstName || !lastName || !email || !password || !confirmPassword) {
      setError({
        title: "Invalid input",
        message: "Please fill out all inputs.",
      });
      return;
    }

    if (password.trim().length < 8) {
      setError({
        title: "Invalid input",
        message:
          "Password is too week. make sure that is longer then 7 characters.",
      });
      return;
    }
    if (password !== confirmPassword) {
      setError({
        title: "Invalid input",
        message:
          "Please make sure password and confirm password are identical.",
      });
      return;
    }

    if (users.filter((user) => user.email === email)) {
      setError({
        title: "Invalid input",
        message:
          "This email alredy exist, please sign up with different email.",
      });
      return;
    }

    const newUser = {
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
    };

    addUser(newUser);
    curUser(newUser);

    setFormFields(defaultFormFields);
    navigate("/");
  };

  const errorHandler = () => {
    setError(null);
  };

  console.log(users);

  return (
    <>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card>
        <form onSubmit={handleSubmit} className="signUpForm">
          <h2>Don't have an account?</h2>
          <span>Sign up with your name, email and password</span>
          <FormInput
            label="Name"
            className="input"
            onChange={handleChange}
            type="text"
            placeholder="Name"
            value={firstName}
            id="firstNameSignUp"
            name="firstName"
          />
          <FormInput
            label="Last Name"
            className="input"
            onChange={handleChange}
            type="text"
            placeholder="Last name"
            value={lastName}
            id="lastNameSignUp"
            name="lastName"
          />
          <FormInput
            label="Email"
            className="input"
            onChange={handleChange}
            type="email"
            placeholder="Email"
            value={email}
            id="emailSignUp"
            name="email"
          />
          <FormInput
            label="Password"
            className="input"
            onChange={handleChange}
            type="password"
            placeholder="password"
            value={password}
            id="passwordSignUp"
            name="password"
          />
          <FormInput
            label="Confirm Password"
            className="input"
            onChange={handleChange}
            type="password"
            placeholder="Confirm Password"
            value={confirmPassword}
            id="confirmPasswordSignUp"
            name="confirmPassword"
          />
          <Button type="submit">Sign Up</Button>
        </form>
      </Card>
    </>
  );
}

export default SignUpForm;
