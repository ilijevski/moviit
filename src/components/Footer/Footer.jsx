import React from "react";
import { Link } from "react-router-dom";

import { FaTruck } from "react-icons/fa";

import "./Footer.scss";

function Footer() {
  return (
    <div className="footer-container">
      <Link className="logo-container" to="/">
        <FaTruck className="logo" />
      </Link>
      <p>Made by Kosta Ilijevski</p>
    </div>
  );
}

export default Footer;
