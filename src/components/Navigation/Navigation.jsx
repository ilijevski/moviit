import React, { useContext } from "react";
import { Link } from "react-router-dom";
import usersContext from "../context/usersContext";

import { FaTruck } from "react-icons/fa";

import "./Navigation.scss";

function Navigation() {
  const { handleSignOut } = useContext(usersContext);

  let curUser = localStorage.getItem("curUser");

  return (
    <div className="navigation">
      <Link className="logo-container" to="/">
        <FaTruck className="logo" />
        <span className="title">MoveIT</span>
      </Link>
      <div className="navigation-link-container">
        {!curUser && (
          <Link className="navigation-link" to="/signInSignUp">
            Sign In
          </Link>
        )}
        {curUser && (
          <div>
            <Link className="navigation-link" to="/orders">
              Orders
            </Link>
            <Link className="navigation-link" to="/" onClick={handleSignOut}>
              Sign Out
            </Link>
          </div>
        )}
      </div>
    </div>
  );
}

export default Navigation;
