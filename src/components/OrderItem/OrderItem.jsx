import React from "react";

import Card from "../Card/Card";

import "./OrderItem.scss";

function OrderItem({ userOrder, curUser }) {
  const {
    livingArea,
    atticArea,
    distance,
    from,
    to,
    orderNumber,
    piano,
    price,
  } = userOrder;
  const { firstName, lastName, email } = curUser;

  return (
    <div className="order-item-container">
      <Card>
        <p>Order Number: {orderNumber}</p>
        <p>
          Full Name: {firstName} {lastName}
        </p>
        <p>Email: {email}</p>
        <p>From: {from}</p>
        <p>To: {to}</p>
        <p>Distance: {distance} &#13214;</p>
        <p>Living area: {livingArea} &#13217;</p>
        <p>Attic area: {atticArea} &#13217;</p>
        <p>Piano: {piano ? 5000 : 0} &#107;&#114;</p>
        <h2>Total Price: {price} &#107;&#114;</h2>
      </Card>
    </div>
  );
}

export default OrderItem;
