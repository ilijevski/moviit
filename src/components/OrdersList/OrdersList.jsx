import React, { useContext, useEffect } from "react";

import UsersContext from "../context/usersContext";
import OrderItem from "../OrderItem/OrderItem";

function OrdersList() {
  const { allOrders, curUserOrder } = useContext(UsersContext);

  const curUser = JSON.parse(localStorage.getItem("curUser"));

  const userOrders = allOrders.filter((order) => curUser.id === order.userId);

  useEffect(() => {
    curUserOrder();
  }, []);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
      }}
    >
      {userOrders.length === 0 && (
        <h2
          style={{
            color: "#f0ebd8",
          }}
        >
          No orders
        </h2>
      )}
      {userOrders.map((userOrder) => (
        <OrderItem key={userOrder.id} curUser={curUser} userOrder={userOrder} />
      ))}
    </div>
  );
}

export default OrdersList;
