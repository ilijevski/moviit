import React from "react";

import Card from "../Card/Card";

import "./GalleryItem.scss";

function GalleryItem({ image }) {
  return (
    <Card>
      <div className="about-image-container">
        <img className="about-image" src={image} alt="" />
      </div>
    </Card>
  );
}

export default GalleryItem;
