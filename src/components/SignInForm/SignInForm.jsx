import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import Card from "../Card/Card";
import FormInput from "../FormInput/FormInput";
import Button from "../Button/Button";
import UsersContext from "../context/usersContext";
import ErrorModal from "../ErrorModal/ErrorModal";

const defaultFormFields = {
  email: "",
  password: "",
};

function SignInForm() {
  const navigate = useNavigate();
  const [formFields, setFormFields] = useState(defaultFormFields);
  const [error, setError] = useState("");
  const { email, password } = formFields;
  const { curUser } = useContext(UsersContext);

  const handleChange = (event) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!email || !password) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid email and password .",
      });
      return;
    }

    const signInUser = {
      email,
      password,
    };
    curUser(signInUser);

    setFormFields(defaultFormFields);
    navigate("/");
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card dark={true}>
        <form onSubmit={handleSubmit}>
          <h2>Already have an account?</h2>
          <span>Sign in with your email and password</span>
          <FormInput
            label="Display Email"
            className="input"
            onChange={handleChange}
            type="email"
            placeholder="Display email"
            value={email}
            id="signInEmail"
            name="email"
          />
          <FormInput
            label="Display Password"
            className="input"
            onChange={handleChange}
            type="password"
            placeholder="Display password"
            value={password}
            id="signInPassword"
            name="password"
          />
          <Button type="submit">Sign In</Button>
        </form>
      </Card>
    </>
  );
}

export default SignInForm;
