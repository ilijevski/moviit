import React, { useState, useContext, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import { useNavigate } from "react-router-dom";

import FormInput from "../FormInput/FormInput";
import Button from "../Button/Button";
import ErrorModal from "../ErrorModal/ErrorModal";

import "./PriceCalculationForm.scss";
import FormInputCheckbox from "../FormInputCheckbox/FormInputCheckbox";
import UsersContext from "../context/usersContext";

const defaultFormFields = {
  from: "",
  to: "",
  distance: "",
  livingArea: "",
  atticArea: "",
  piano: false,
};

const calcPrice = (distance, livingArea, atticArea, piano) => {
  let distancePrice = 0;
  let loadPrice = 0;
  let pianoPrice = 0;
  let price = 0;
  let numberOfCars = 0;

  numberOfCars = Math.ceil(Number(livingArea) / 50 + Number(atticArea) / 25);

  if (distance < 50) {
    distancePrice = 1000 + distance * 10;
  } else if (distance >= 50 && distance <= 100) {
    distancePrice = 5000 + distance * 8;
  } else if (distance > 100) {
    distancePrice = 10000 + distance * 7;
  }

  pianoPrice = piano ? 5000 : 0;

  loadPrice = Number(
    Math.ceil(Number(livingArea) / 50 + Number(atticArea) / 25) * 1100
  );

  price = distancePrice * numberOfCars + loadPrice + pianoPrice;

  return price;
};

function PriceCalculationForm() {
  const { addOrder } = useContext(UsersContext);
  const [formFields, setFormFields] = useState(defaultFormFields);
  const [error, setError] = useState("");
  const [piano, setPiano] = useState(false);

  const { from, to, distance, livingArea, atticArea } = formFields;

  const currentUser = JSON.parse(localStorage.getItem("curUser"));

  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };

  const handleChecked = () => {
    setPiano(!piano);
  };

  useEffect(() => {
    const penOrder = JSON.parse(localStorage.getItem("pendingOrder"));
    if (penOrder) {
      setFormFields({ ...penOrder });
    }
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!from || !to || !distance || !livingArea || !atticArea) {
      setError({
        title: "Invalid input",
        message: "Please fill out all inputs.",
      });
      return;
    }

    if (distance < 0 || livingArea < 0 || atticArea < 0) {
      setError({
        title: "Invalid input",
        message:
          "Distance, living area and attic area must be positive numbers.",
      });
      return;
    }

    if (currentUser) {
      if (localStorage["pendingOrder"]) {
        let pendingOrder = JSON.parse(localStorage.getItem("pendingOrder"));

        setFormFields({ ...pendingOrder });
        const newOrder = {
          ...pendingOrder,
          userId: currentUser.id,
        };

        addOrder(newOrder);
        setFormFields(defaultFormFields);
        localStorage.removeItem("pendingOrder");
        navigate("/orders");
      } else if (!localStorage["pendingOrder"]) {
        const newOrder = {
          from,
          to,
          distance,
          livingArea,
          atticArea,
          piano,
          price: calcPrice(distance, livingArea, atticArea, piano),
          userId: currentUser.id,
          orderNumber: uuidv4(),
        };

        addOrder(newOrder);
        navigate("/orders");

        setFormFields(defaultFormFields);
      }
    }

    if (!currentUser) {
      const newOrder = {
        from,
        to,
        distance,
        livingArea,
        atticArea,
        piano,
        price: calcPrice(distance, livingArea, atticArea, piano),
        orderNumber: uuidv4(),
      };

      localStorage.setItem("pendingOrder", JSON.stringify(newOrder));

      setError({
        title: "Pending Order",
        message: `Total price is: ${newOrder.price} kr, to place order sign in first.`,
      });
    }
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <div className="price-calculation">
        <h2 className="price-calculation-title">Price calculation</h2>
        <div className="form-container">
          <form onSubmit={handleSubmit} className="price-calculation-form">
            <FormInput
              label="From"
              type="text"
              placeholder="From"
              value={from}
              onChange={handleChange}
              id="from"
              name="from"
            />
            <FormInput
              label="To"
              type="text"
              placeholder="To"
              value={to}
              onChange={handleChange}
              id="to"
              name="to"
            />
            <FormInput
              label="Distance"
              type="number"
              placeholder="Distance"
              value={distance}
              onChange={handleChange}
              id="distance"
              name="distance"
            />
            <FormInput
              label="Living Area"
              type="number"
              placeholder="Living Area"
              value={livingArea}
              onChange={handleChange}
              id="livingArea"
              name="livingArea"
            />
            <FormInput
              label="Attic/Basement Area"
              type="number"
              placeholder="Attic/Basement Area"
              value={atticArea}
              onChange={handleChange}
              id="atticArea"
              name="atticArea"
            />
            <FormInputCheckbox
              label="Piano"
              type="checkbox"
              value={piano}
              onChange={handleChecked}
              id="piano"
              name="piano"
            />
            <Button type="submit">
              {currentUser ? "Place order" : "Check price"}
            </Button>
          </form>
        </div>
      </div>
    </>
  );
}

export default PriceCalculationForm;
