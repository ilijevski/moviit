import React from "react";
import ReactDOM from "react-dom";

import "./ErrorModal.scss";

const Backdrop = ({ onConfirm }) => {
  return <div className="backdrop" onClick={onConfirm} />;
};

const ModalOverlay = ({ title, message }) => {
  return (
    <div className="modal">
      <header className="modal-header">
        <h2>{title}</h2>
      </header>
      <div className="modal-content">
        <p>{message}</p>
      </div>
    </div>
  );
};

const ErrorModal = (props) => {
  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop onConfirm={props.onConfirm} />,
        document.getElementById("backdrop-root")
      )}
      {ReactDOM.createPortal(
        <ModalOverlay title={props.title} message={props.message} />,
        document.getElementById("overlay-root")
      )}
    </>
  );
};

export default ErrorModal;
