import React from "react";

import "./AboutText.scss";

function AboutText() {
  return (
    <>
      <div className="about-text">
        <h3 className="about-text-heading">Why Us</h3>
        <p className="about-text-para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio
          praesentium tempora numquam eaque ut voluptatem excepturi facere sed
          placeat magnam!
        </p>
        <h3 className="about-text-heading">No hidden costs</h3>
        <p className="about-text-para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates
          harum amet ut ipsa, excepturi facilis delectus recusandae consequuntur
          quia? Nisi illo rem voluptate obcaecati similique repudiandae fugit
          quas, voluptatum maiores!
        </p>
        <h3 className="about-text-heading">Years of proven history</h3>
        <p className="about-text-para">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis
          deleniti incidunt, quasi blanditiis molestias rem corrupti maxime
          ipsam nobis sed ratione placeat? Alias, debitis iure?
        </p>
      </div>
    </>
  );
}

export default AboutText;
