import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from "./Pages/Home/Home";
import SignInSignUp from "./Pages/SignInSignUp/SignInSignUp";
import Navigation from "./components/Navigation/Navigation";
import Orders from "./Pages/Orders/Orders";
import { UsersProvider } from "./components/context/usersContext";

import "./App.scss";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <UsersProvider>
      <Router>
        <Navigation />
        <Routes>
          <Route path="/" index element={<Home />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/signInSignUp" element={<SignInSignUp />} />
        </Routes>
        <Footer />
      </Router>
    </UsersProvider>
  );
}

export default App;
