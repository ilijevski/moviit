import React from "react";

import Header from "../../components/Header/Header";
import About from "../../components/About/About";
import PriceCalculationForm from "../../components/PriceCalculationForm/PriceCalculationForm";

import "./Home.scss";
import Gallery from "../../components/Gallery/Gallery";

function Home() {
  return (
    <div className="home">
      <Header />
      <About />
      <PriceCalculationForm />
      <Gallery />
    </div>
  );
}

export default Home;
