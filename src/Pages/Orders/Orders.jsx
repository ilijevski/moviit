import React from "react";

import OrdersList from "../../components/OrdersList/OrdersList";

import "./Orders.scss";

function Orders() {
  return (
    <div className="orders">
      <OrdersList />
    </div>
  );
}

export default Orders;
