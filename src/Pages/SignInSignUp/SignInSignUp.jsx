import React from "react";

import SignUpForm from "../../components/SignUpForm/SignUpForm";
import SignInForm from "../../components/SignInForm/SignInForm";

import "./SignInSignUp.scss";

function SignInSignUp() {
  return (
    <div className="signInSignUp">
      <SignUpForm />
      <SignInForm />
    </div>
  );
}

export default SignInSignUp;
